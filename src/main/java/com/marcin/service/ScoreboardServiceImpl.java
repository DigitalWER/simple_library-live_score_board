package com.marcin.service;

import com.marcin.exception.MatchNotFoundException;
import com.marcin.model.Match;
import com.marcin.model.Scoreboard;
import com.marcin.model.Team;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ScoreboardServiceImpl implements ScoreboardService {

    private final Scoreboard scoreboard;
    private int matchIdIterator = 0;

    public ScoreboardServiceImpl(Scoreboard scoreboard) {
        this.scoreboard = scoreboard;
    }

    @Override
    public Match startNewMatch(Team homeTeam, Team awayTeam) {
        var newMatch = new Match(generateMatchId(), homeTeam, awayTeam);
        scoreboard.getMatches().add(newMatch);
        return newMatch;
    }

    @Override
    public Match getMatchById(int matchId) {
        return scoreboard.getMatches().stream()
                .filter(match -> match.getMatchId().equals(matchId))
                .findFirst()
                .orElseThrow(() -> new MatchNotFoundException("Match with id:" + matchId + " does not exist"));
    }

    @Override
    public void finishMostRecentMatch() {
        var matches = scoreboard.getMatches();
        matches.remove(matches.size() - 1);
    }

    @Override
    public void finishTheOldestMatch() {
        var matches = scoreboard.getMatches();
        matches.remove(0);
    }

    @Override
    public void finishAllMatches() {
        var matches = scoreboard.getMatches();
        matches.clear();
    }

    @Override
    public void finishMatchById(Integer matchId) {
        var matches = scoreboard.getMatches();
        matches.remove(getMatchById(matchId));
    }

    @Override
    public void finishMatchesByIds(List<Integer> ids) {
        var matches = scoreboard.getMatches();
        for (int matchId : ids) {
            matches.remove(getMatchById(matchId));
        }
    }

    @Override
    public void finishMatchByIndex(int index) {
        var matches = scoreboard.getMatches();
        try {
            matches.remove(index);
        } catch (Exception ex) {
            throw new MatchNotFoundException("Match with index:" + index + " does not exist");
        }

    }

    @Override
    public String getScoreboardSummary() {
        var sortedMatches = getSortedMatches();
        return generateScoreboardSummary(sortedMatches);
    }

    private int generateMatchId() {
        matchIdIterator = matchIdIterator + 1;
        return matchIdIterator;
    }

    private List<Match> getSortedMatches() {
        var matches = scoreboard.getMatches();
        Collections.reverse(matches);
        return matches.stream()
                .sorted(Comparator.comparing(Match::getTotalScore).reversed())
                .toList();
    }

    private String generateScoreboardSummary(List<Match> sortedMatches) {
        StringBuilder sbMatchSummary = new StringBuilder();
        sbMatchSummary.append("Match summary:\n");

        for (Match match : sortedMatches) {
            sbMatchSummary.append(match.getCurrentScore());
            if (!(match == sortedMatches.get(sortedMatches.size() - 1))) {
                sbMatchSummary.append("\n");
            }
        }
        return sbMatchSummary.toString();
    }
}
