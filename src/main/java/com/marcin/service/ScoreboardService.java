package com.marcin.service;

import com.marcin.exception.MatchNotFoundException;
import com.marcin.model.Match;
import com.marcin.model.Scoreboard;
import com.marcin.model.Team;

import java.util.List;

public interface ScoreboardService {
    /**
     * Method responsible for starting a new match, which will be added to the Scoreboard
     *
     * @param homeTeam Team object that represents HomeTeam
     * @param awayTeam Team object that represents AwayTeam
     * @return new {@link Match} object that has been created and added to thew Scoreboard
     */
    Match startNewMatch(Team homeTeam, Team awayTeam);

    /**
     * Simply getting match by its id. When trying to get a match by id that cannot be found,
     * then {@link MatchNotFoundException} will be thrown
     *
     * @param matchId Provided id of the desired match
     * @return Found {@link Match} object
     */
    Match getMatchById(int matchId);

    /**
     * Removes the most recent {@link Match} from the {@link Scoreboard}
     */
    void finishMostRecentMatch();

    /**
     * Removes the oldest {@link Match} from the {@link Scoreboard}
     */
    void finishTheOldestMatch();

    /**
     * Removes all {@link Match} objects from the {@link Scoreboard}
     */
    void finishAllMatches();

    /**
     * Removes chosen {@link Match} by its id from the {@link Scoreboard}
     *
     * @param matchId Provided id to find {@link Match} object.
     */
    void finishMatchById(Integer matchId);

    /**
     * Removes several chosen {@link Match} objects from the {@link Scoreboard} by its ids provided inside the List parameter
     *
     * @param ids List of matchIds
     */
    void finishMatchesByIds(List<Integer> ids);

    /**
     * Removes chosen {@link Match} by its index from the {@link Scoreboard}
     *
     * @param index Provided index number from the {@link Scoreboard} list
     */
    void finishMatchByIndex(int index);

    /**
     * Get a summary of matches in progress ordered by their total score. Matches with the
     * same total score will be returned ordered by the most recently started match on the
     * scoreboard.
     * <p>
     * Both conditions are met in the {@code getSortedMatches()} method by reversing the whole order of items in the list of matches and then
     * sorting all objects using .sorted() method and a Comparator class which affects the total score filtering
     * (also .reversed() - to get descending sorting outcome). By reversing it at the start, the condition for matches
     * with the same total score in proper order is met. At the end summary is being generated with {@code generateScoreboardSummary()}.
     *
     * @return ready summary for current matches on the Scoreboard
     */
    String getScoreboardSummary();
}
