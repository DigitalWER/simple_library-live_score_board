package com.marcin.model;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
public class Scoreboard {

    private final List<Match> matches = new ArrayList<>();

}
