package com.marcin.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Match {
    private static final String ABSOLUTE_SCORE_PATTERN = "%s %s - %s %s";
    private final Integer matchId;
    private final Team homeTeam;
    private final Team awayTeam;
    private int homeTeamScore;
    private int awayTeamScore;
    private int totalScore;

    public Match(int matchId, Team homeTeam, Team awayTeam) {
        this.matchId = matchId;
        this.homeTeam = homeTeam;
        this.awayTeam = awayTeam;
        homeTeamScore = 0;
        awayTeamScore = 0;
        totalScore = 0;
    }

    public void updateScore(int newHomeTeamScore, int newAwayTeamScore) {
        setHomeTeamScore(newHomeTeamScore);
        setAwayTeamScore(newAwayTeamScore);
        setTotalScore(newHomeTeamScore + newAwayTeamScore);
    }

    public String getCurrentScore() {
        return String.format(ABSOLUTE_SCORE_PATTERN, getHomeTeam().getName(), getHomeTeamScore(), getAwayTeam().getName(), getAwayTeamScore());
    }
}
