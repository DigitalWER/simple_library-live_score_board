package com.marcin.service;

import com.marcin.exception.MatchNotFoundException;
import com.marcin.model.Match;
import com.marcin.model.Scoreboard;
import com.marcin.model.Team;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

class ScoreboardServiceTest {

    private Scoreboard scoreboard;
    private ScoreboardService scoreboardService;

    @BeforeEach
    void init() {
        scoreboard = new Scoreboard();
        scoreboardService = new ScoreboardServiceImpl(scoreboard);
    }

    @Test
    void testStartNewMatch_whenNewMatchCreated_thenHomeTeamAndAwayTeamAreSet() {
        Team homeTeam = new Team("Germany");
        Team awayTeam = new Team("England");

        Match match = scoreboardService.startNewMatch(homeTeam, awayTeam);

        assertEquals(homeTeam, match.getHomeTeam(), "Germany is expected to be a home team");
        assertEquals(awayTeam, match.getAwayTeam(), "England is expected to be an away team");
    }

    @Test
    void testStartNewMatch_whenNewMatchCreated_thenBothScoresExpectedToBeZero() {
        Team firstTeam = new Team("Germany");
        Team secondTeam = new Team("England");
        String expectedAbsoluteScore = String.format("%s %s - %s %s", firstTeam.getName(), 0, secondTeam.getName(), 0);

        Match match = scoreboardService.startNewMatch(firstTeam, secondTeam);

        assertEquals(expectedAbsoluteScore, match.getCurrentScore(), "After match creation both teams should have initial score 0 - 0");
    }

    @Test
    void testStartNewMatch_whenStartingNewMatchAfterRemoval_thenNewMatchHasUniqueId() {
        scoreboardService.startNewMatch(new Team("Germany"), new Team("England"));
        scoreboardService.startNewMatch(new Team("Poland"), new Team("Spain"));
        var thirdMatch = scoreboardService.startNewMatch(new Team("Croatia"), new Team("Italy"));

        scoreboardService.finishMostRecentMatch();
        var newMatch = scoreboardService.startNewMatch(new Team("Greece"), new Team("Czech Republic"));

        assertNotEquals(thirdMatch.getMatchId(), newMatch.getMatchId(), "Compared matches should have different ids!");
    }

    @Test
    void testGetMatches_whenGettingAllMatches_thenShouldReturnArrayWithAllMatches() {
        var firstMatch = scoreboardService.startNewMatch(new Team("Germany"), new Team("England"));
        var secondMatch = scoreboardService.startNewMatch(new Team("Poland"), new Team("Spain"));
        var thirdMatch = scoreboardService.startNewMatch(new Team("Croatia"), new Team("Italy"));
        List<Match> expectedMatches = List.of(firstMatch, secondMatch, thirdMatch);

        List<Match> matches = scoreboard.getMatches();

        assertEquals(expectedMatches, matches, "Some matches might be missing");
    }

    @Test
    void testGetMatchById_whenChoosingMatchById_ExactMatchShouldBeReturned() {
        scoreboardService.startNewMatch(new Team("Germany"), new Team("England"));
        scoreboardService.startNewMatch(new Team("Poland"), new Team("Spain"));
        var expectedMatch = scoreboardService.startNewMatch(new Team("Croatia"), new Team("Italy"));
        int matchId = 3;

        Match retrievedMatch = scoreboardService.getMatchById(matchId);

        assertEquals(expectedMatch, retrievedMatch, "Provided id did not return expected match");
    }

    @Test
    void testGetMatchById_whenChoosingMatchByIdThatDoesNotExist_thenMatchNotFoundExceptionShouldBeThrown() {
        startTestMatches();

        int matchId = 10;

        assertNotNull(scoreboard.getMatches());
        assertThrows(MatchNotFoundException.class, () -> scoreboardService.getMatchById(matchId));
    }

    @Test
    void testFinishMatchesByIds_whenFinishingMatchesByIdsWithSingleId_thenRemoveProvidedMatchFromTheScoreboard() {
        startTestMatches();

        scoreboardService.finishMatchById(3);

        assertEquals(2, scoreboard.getMatches().size(), "After finishing the match there should be only 2 more matches in progress");
    }

    @Test
    void testFinishMatchesByIds_whenFinishingMatchesByIds_thenRemoveChosenMatchesFromTheScoreboard() {
        startTestMatches();

        scoreboardService.finishMatchesByIds(List.of(1, 3));

        assertEquals(1, scoreboard.getMatches().size(), "After finishing the match there should be only 2 more matches in progress");
    }

    @Test
    void testFinishMostRecentMatch_whenUsingFinishMostRecentMatch_thenRecentMatchShouldBeRemovedFromAllMatchesOnTheScoreboard() {
        startTestMatches();

        scoreboardService.finishMostRecentMatch();

        assertEquals(2, scoreboard.getMatches().size(), "After finishing the match there should be only 2 more matches in progress");
    }

    @Test
    void testFinishTheOldestMatch_whenUsingFinishTheOldestMatch_thenTheOldestMatchShouldBeRemovedFromAllMatchesOnTheScoreboard() {
        startTestMatches();

        scoreboardService.finishTheOldestMatch();

        assertEquals(2, scoreboard.getMatches().size(), "After finishing the match there should be only 2 more matches in progress");
    }

    @Test
    void testFinishAllMatches_whenFinishAllMatches_thenClearTheScoreboard() {
        startTestMatches();

        scoreboardService.finishAllMatches();

        assertEquals(0, scoreboard.getMatches().size(), "After finishing all matches there should be no matches available on the scoreboard");
    }

    @Test
    void testFinishMatchByIndex_whenFinishingByIndex_thenConfirmChosenMatchWasRemovedFromScoreboard() {
        startTestMatches();

        scoreboardService.finishMatchByIndex(2);

        assertEquals(2, scoreboard.getMatches().size(), "After finishing the match there should be only 2 more matches in progress");
    }

    @Test
    void testFinishMatchByIndex_whenFinishingByIndexWhichIsLargerThanSizeOfTheScoreboard_thenThrowMatchNotFoundException() {
        startTestMatches();

        int index = 10;

        assertNotNull(scoreboard.getMatches());
        assertThrows(MatchNotFoundException.class, () -> scoreboardService.finishMatchByIndex(index));
    }

    @Test
    void testGetSummaryOfMatches_whenGettingSummaryOfScoreboard_thenReturnAllMatchesInProgressWithAnAbsoluteScoreOrderedByTheirTotalScoreWithMostRecentMatchRule() {
        var firstMatch = scoreboardService.startNewMatch(new Team("Mexico"), new Team("Canada"));
        var secondMatch = scoreboardService.startNewMatch(new Team("Spain"), new Team("Brazil"));
        var thirdMatch = scoreboardService.startNewMatch(new Team("Germany"), new Team("France"));
        var fourthMatch = scoreboardService.startNewMatch(new Team("Uruguay"), new Team("Italy"));
        var fifthMatch = scoreboardService.startNewMatch(new Team("Argentina"), new Team("Australia"));

        firstMatch.updateScore(0, 5);
        secondMatch.updateScore(10, 2);
        thirdMatch.updateScore(2, 2);
        fourthMatch.updateScore(6, 6);
        fifthMatch.updateScore(3, 1);

        var expectedMatchSummary = "Match summary:\n" +
                fourthMatch.getCurrentScore() + "\n" +
                secondMatch.getCurrentScore() + "\n" +
                firstMatch.getCurrentScore() + "\n" +
                fifthMatch.getCurrentScore() + "\n" +
                thirdMatch.getCurrentScore();

        String matchSummary = scoreboardService.getScoreboardSummary();

        String errorMessage = String.format("Match summaries are not the same! \nExpected: \n%s, \n\nbut got: \n%s", expectedMatchSummary, matchSummary);
        assertEquals(expectedMatchSummary, matchSummary, errorMessage);
    }

    private void startTestMatches() {
        scoreboardService.startNewMatch(new Team("Germany"), new Team("England"));
        scoreboardService.startNewMatch(new Team("Poland"), new Team("Spain"));
        scoreboardService.startNewMatch(new Team("Croatia"), new Team("Italy"));
    }
}
