package com.marcin.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MatchTest {
    private static final String ABSOLUTE_SCORE_PATTERN = "%s %s - %s %s";
    private final Team homeTeam = new Team("Portugal");
    private final Team awayTeam = new Team("Spain");
    private final Match match = new Match(1, homeTeam, awayTeam);

    @Test
    void testUpdateScore_whenUpdatingBothScoresToMatch_thenNewPairOfAbsoluteScoresWithBothUpdatedScoresShouldBeReturned() {
        int newHomeTeamScore = 1;
        int newAwayTeamScore = 2;
        String expectedNewAbsoluteScore = String.format(ABSOLUTE_SCORE_PATTERN, homeTeam.getName(), newHomeTeamScore, awayTeam.getName(), newAwayTeamScore);

        match.updateScore(newHomeTeamScore, newAwayTeamScore);

        assertEquals(expectedNewAbsoluteScore, match.getCurrentScore(), "Returned absolute score after update is not matching expected result");
    }
}
