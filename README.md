# Simple Library Live Score Board

Author: Marcin Wilczyński   
Current Role: Software Engineer Associate

## Exercise assessment:

<details>
  <summary></summary>

The scoreboard supports the following operations:

1. Start a new match, assuming initial score 0 – 0 and adding it the scoreboard.
   This should capture following parameters:
   a. Home team
   b. Away team
2. Update score. This should receive a pair of absolute scores: home team score and away
   team score.
3. Finish match currently in progress. This removes a match from the scoreboard.
4. Get a summary of matches in progress ordered by their total score. The matches with the
   same total score will be returned ordered by the most recently started match in the
   scoreboard.

</details>

## Introduction

The whole project has been handled with the usage of Java 17 with maven to inject Junit and Lombok dependency
for good test quality and code simplification in case of annotations. The whole project was performed with the TDD
approach
with suggestions provided in the pdf assessment file.

## Project Overview

The whole project is very simple and contains just library implementation. The presented library is combined from
3 model elements, one "service" layer, and a custom exception.  
Models are as follows:

- <b>Scoreboard</b>
    - Main and theme POJO class which includes just a List of Match objects. Operations on that object are implemented
      in the <B>ScoreboardService</b> class.
- <b>Match</b>
    - POJO class that is being directly injected into Scoreboard after creation. Its purpose is to hold all information
      about teams and score till the end of the match
- <b>Team</b>
    - POJO class that represents a team

The service layer contains previously mentioned ScoreboardService, which is the main operation layer over the
Scoreboard.class. That includes all required operations like:

- starting a new match, so it is presented on the Scoreboard
- finding particular match
- finishing match (the oldest/most recent one, all of them or selected by matchId)
- generation of Scoreboard summary - presentation of all matches that are currently in progress

Match.class does not have its Service layer for operations, as for current implementation it was only needed to have an
updating score,
which is something like an "advanced" setter, and get the current score - which is slightly like a custom toString()
method - both are in the POJO frames

Because of the simplicity, there is only a single exception available - <b>MatchNotFoundException</b>. That one has a
purpose while trying to get a particular match
by its ID - if a match with the provided ID does not exist, then it is being handled by such an exception.

### Edge cases

- Finishing a match - that was handled in a few different ways
    - The most recent one
    - The oldest one
    - Finishing by ID
    - Finishing by index
    - Finishing all
- Exception handling in case that particular match cannot be found
- totalScore variable in Match.class - increases code readability rather than calculating it on the fly while sorting
  Scoreboard

## Project Summary & Suggestions

Current implementation is ready-to-use/-improve library that can be instantly applied to different applications
depending on the requirements.
From a larger perspective, it is possible to expand it by different implementations of ScoreboardService or inheritance
between model classes to get
more accurate objects if we would like to design scoreboards that should be themed.

It is a valuable exercise to perform in case of training the TDD approach as well as constantly improving code quality
skills in practice.

From a personal point of view, I decided to go with such an approach which would be quite simple to implement into
basic service operations - so possibility to use it as REST API. It would also be possible to add CRUD operations and
store data in the database.
Another thing that came to my mind was to create an enum for Match.class, which would have stages like TO_START,
IN_PROGRESS, or FINISHED,
but in my opinion that could be a part of datasource implementation when we could also consider adding a new feature -
history of Scoreboard
that in practice would be very handy, especially in case of data handling.

## Others

<details>
  <summary>Gitlab Getting Started</summary>

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/DigitalWER/simple_library-live_score_board.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/DigitalWER/simple_library-live_score_board/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thanks to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README

Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.

</details>


